﻿namespace HS.Persistence.EntityFramework
{
    using System.Threading;
    using System.Threading.Tasks;

    using HS.Domain.Common;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;

    public class UnitOfWork<TDbContext> : IUnitOfWork where TDbContext : DbContext
    {
        private bool _isDisposed = false;
        protected IDbContextTransaction _transaction;
        protected volatile int _transactionDepth = 0;

        public UnitOfWork(TDbContext dbContext )
        {
            DbContext = dbContext;
            GenericRepository = new GenericRepository<TDbContext>(dbContext);
            ReadOnlyRepository = new ReadOnlyRepository<TDbContext>(dbContext);
        }

        public virtual IGenericRepository GenericRepository { get; }
        public virtual IReadOnlyRepository ReadOnlyRepository { get; }

        protected TDbContext DbContext { get; }

        public virtual void BeginTransaction()
        {
            Interlocked.Increment(ref _transactionDepth);
            if(_transactionDepth == 1)
            {
                _transaction = DbContext.Database.BeginTransaction();
            }
        }

        public virtual async Task BeginTransactionAsync(CancellationToken token = default)
        {
            Interlocked.Increment(ref _transactionDepth);
            if(_transactionDepth == 1)
            {
                _transaction = await DbContext.Database.BeginTransactionAsync(token);
            }
        }

        public virtual void Commit()
        {
            Save();

            if(_transactionDepth == 1)
            {
                Interlocked.Exchange(ref _transactionDepth, 0);

                _transaction?.Commit();
                _transaction?.Dispose();
                _transaction = null;

                return;
            }

            if(_transactionDepth > 0)
            {
                Interlocked.Decrement(ref _transactionDepth);
            }
        }

        public virtual async Task CommitAsync(CancellationToken token = default)
        {
            await SaveAsync(token);

            if(_transactionDepth == 1)
            {
                Interlocked.Exchange(ref _transactionDepth, 0);
                if(_transaction != null)
                {
                    await _transaction.CommitAsync(token);
                    await _transaction.DisposeAsync();
                    _transaction = null;
                }
            }

            if(_transactionDepth > 0)
            {
                Interlocked.Decrement(ref _transactionDepth);
            }
        }

        public virtual void Rollback()
        {
            if(_transactionDepth == 1)
            {
                Interlocked.Exchange(ref _transactionDepth, 0);

                _transaction?.Rollback();
                _transaction?.Dispose();
                _transaction = null;

                foreach(var entry in DbContext.ChangeTracker.Entries())
                {
                    entry.Reload();
                }

                DbContext?.Database?.RollbackTransaction();

                return;
            }

            if(_transactionDepth > 0)
            {
                Interlocked.Decrement(ref _transactionDepth);
            }
        }

        public virtual async Task RollbackAsync(CancellationToken token = default)
        {
            if(_transactionDepth == 1)
            {
                Interlocked.Exchange(ref _transactionDepth, 0);
                if(_transaction != null)
                {
                    await _transaction.RollbackAsync(token);
                    await _transaction.DisposeAsync();

                    _transaction = null;
                }

                foreach(var entry in DbContext.ChangeTracker.Entries())
                {
                    entry.Reload();
                }
            }

            if(_transactionDepth > 0)
            {
                Interlocked.Decrement(ref _transactionDepth);
            }
        }

        public virtual void Save()
        {
            DbContext?.SaveChanges();
        }

        public virtual async Task SaveAsync(CancellationToken token = default)
        {
            await DbContext.SaveChangesAsync(token);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(!_isDisposed)
            {
                if(disposing)
                {
                    _transaction?.Dispose();
                    DbContext?.Dispose();
                }

                _isDisposed = true;
            }
        }

        public virtual void Dispose()
        {
            Dispose(true);
        }
    }
}