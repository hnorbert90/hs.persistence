﻿namespace HS.Persistence.EntityFramework
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using System.Threading.Tasks;

    using HS.Domain.Common;

    using Microsoft.EntityFrameworkCore;

    public class ReadOnlyRepository<TDbContext> : IReadOnlyRepository where TDbContext : DbContext

    {
        public ReadOnlyRepository(TDbContext dBContext)
        {
            DbContext = dBContext;
        }

        protected TDbContext DbContext { get; }

        public virtual long Count<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class, new()
        {
            var query = GetFilteredDbSet(filter);

            return query.LongCount();
        }

        public virtual Task<long> CountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null, CancellationToken token = default) where TEntity : class, new()
        {
            var query = GetFilteredDbSet(filter);

            return query.LongCountAsync(token);
        }

        public virtual IQueryable<TEntity> Query<TEntity>() where TEntity : class, new()
        {
            return DbContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, new()
        {
            var query = GetFilteredDbSet(filter);

            return query;
        }

        public virtual IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy) where TEntity : class, new()
        {
            var query = GetFilteredDbSet(filter);

            query = orderBy != null ? orderBy(query) : query;

            return query;
        }

        public virtual TEntity Read<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new()
        {
           return DbContext.Set<TEntity>().FirstOrDefault(where);
        }

        public virtual Task<TEntity> ReadAsync<TEntity>(Expression<Func<TEntity, bool>> where, CancellationToken token = default) where TEntity : class, new()
        {
            return DbContext.Set<TEntity>().FirstOrDefaultAsync(where, token);
        }

        private IQueryable<TEntity> GetFilteredDbSet<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, new()
        {
            return filter == null
                ? DbContext.Set<TEntity>()
                : DbContext.Set<TEntity>().Where(filter);
        }
    }

    public class ReadOnlyRepository<TDbContext, TEntity> : IReadOnlyRepository<TEntity>
        where TDbContext : DbContext
        where TEntity : class, new()
    {
        protected readonly IReadOnlyRepository _readOnlyRepository;
        protected TDbContext _dbContext;

        public ReadOnlyRepository(TDbContext dbContext)
        {
            _dbContext = dbContext;
            _readOnlyRepository = new ReadOnlyRepository<TDbContext>(dbContext);
        }

        public virtual IQueryable<TEntity> Set { get; }

        public virtual long Count(Expression<Func<TEntity, bool>> filter = null)
        {
            return _readOnlyRepository.Count(filter);
        }

        public virtual Task<long> CountAsync(Expression<Func<TEntity, bool>> filter = null, CancellationToken token = default)
        {
            return _readOnlyRepository.CountAsync(filter, token);
        }

        public virtual IQueryable<TEntity> Query()
        {
            return _readOnlyRepository.Query<TEntity>();
        }

        public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter)
        {
            return _readOnlyRepository.Query(filter);
        }

        public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy)
        {
            return _readOnlyRepository.Query(filter, orderBy);
        }

        public virtual TEntity Read(Expression<Func<TEntity, bool>> where)
        {
            return _readOnlyRepository.Read(where);
        }

        public virtual Task<TEntity> ReadAsync(Expression<Func<TEntity, bool>> where, CancellationToken token = default)
        {
            return _readOnlyRepository.ReadAsync(where, token);
        }
    }
 }