﻿namespace HS.Persistence.EntityFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using System.Threading.Tasks;

    using HS.Domain.Common;

    using Microsoft.EntityFrameworkCore;

    public class GenericRepository<TDbContext> : ReadOnlyRepository<TDbContext>, IGenericRepository where TDbContext : DbContext
    {
        public GenericRepository(TDbContext dbContext) : base(dbContext)
        {
        }

        public virtual void Create<TEntity>(TEntity entity) where TEntity : class, new()
        {
            DbContext.Add(entity);
        }

        public virtual async Task CreateAsync<TEntity>(TEntity entity, CancellationToken token = default) where TEntity : class, new()
        {
            await DbContext.AddAsync(entity, token);
        }

        public virtual void CreateMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, new()
        {
            DbContext.AddRange(entities);
        }

        public virtual async Task CreateManyAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken token = default) where TEntity : class, new()
        {
            await DbContext.AddRangeAsync(entities, token);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class, new()
        {
            if(entity is ILogicallyDeletable logicallyDeletableEntity)
            {
                logicallyDeletableEntity.DeletedOn = DateTime.UtcNow;
                logicallyDeletableEntity.IsDeleted = true;
            }
            else
            {
                DbContext.Remove(entity);
            }
        }

        public virtual void Delete<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new()
        {
            if (where is null)
            {
                throw new ArgumentNullException(nameof(where));
            }

            DeleteMany(DbContext.Set<TEntity>().Where(where));
        }

        public virtual Task DeleteAsync<TEntity>(Expression<Func<TEntity, bool>> where, CancellationToken token = default) where TEntity : class, new()
        {
            Delete(where);

            return Task.CompletedTask;
        }

        public virtual void DeleteMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, new()
        {
            if(typeof(TEntity).GetInterfaces().Any(x => x == typeof(ILogicallyDeletable)))
            {
                foreach (var entity in entities)
                {
                    Delete(entity);
                }
            }
            else
            {
                DbContext.RemoveRange(entities);
            }
        }

        public virtual void Save()
        {
            DbContext.SaveChanges();
        }

        public virtual Task SaveAsync(CancellationToken token = default)
        {
            return DbContext.SaveChangesAsync(token);
        }

        public virtual void Update<TEntity>(TEntity entity) where TEntity : class, new()
        {
            DbContext.Update(entity);
        }

        public virtual void Update<TEntity>(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public virtual Task UpdateAsync<TEntity>(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null, CancellationToken token = default) where TEntity : class, new()
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, new()
        {
            DbContext.UpdateRange(entities);
        }
    }

    public class GenericRepository<TDbContext, TEntity> : IRepository<TEntity>
        where TDbContext : DbContext
        where TEntity : class, new()
    {
        protected readonly IGenericRepository _genericRepository;

        public GenericRepository(TDbContext dbContext)
        {
            DbContext = dbContext;
            _genericRepository = new GenericRepository<TDbContext>(DbContext);
        }

        protected TDbContext DbContext { get; }

        public virtual IQueryable<TEntity> Set { get; }

        public virtual long Count(Expression<Func<TEntity, bool>> filter = null)
        {
            return _genericRepository.Count(filter);
        }

        public virtual Task<long> CountAsync(Expression<Func<TEntity, bool>> filter = null, CancellationToken token = default)
        {
            return _genericRepository.CountAsync(filter, token);
        }

        public virtual void Create(TEntity entity)
        {
            _genericRepository.Create(entity);
        }

        public virtual Task CreateAsync(TEntity entity, CancellationToken token = default)
        {
            return _genericRepository.CreateAsync(entity, token);
        }

        public virtual void CreateMany(IEnumerable<TEntity> entities)
        {
            _genericRepository.CreateMany(entities);
        }

        public virtual Task CreateManyAsync(IEnumerable<TEntity> entities, CancellationToken token = default)
        {
            return _genericRepository.CreateManyAsync(entities, token);
        }

        public virtual void Delete(TEntity entity)
        {
            _genericRepository.Delete(entity);
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            _genericRepository.Delete(where);
        }

        public virtual Task DeleteAsync(Expression<Func<TEntity, bool>> where, CancellationToken token = default)
        {
            return _genericRepository.DeleteAsync(where, token);
        }

        public virtual void DeleteMany(IEnumerable<TEntity> entities)
        {
            _genericRepository.DeleteMany(entities);
        }

        public virtual IQueryable<TEntity> Query()
        {
            return _genericRepository.Query<TEntity>();
        }

        public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter)
        {
            return _genericRepository.Query(filter);
        }

        public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy)
        {
            return _genericRepository.Query(filter, orderBy);
        }

        public virtual TEntity Read(Expression<Func<TEntity, bool>> where)
        {
            return _genericRepository.Read(where);
        }

        public virtual Task<TEntity> ReadAsync(Expression<Func<TEntity, bool>> where, CancellationToken token = default)
        {
            return _genericRepository.ReadAsync(where, token);
        }

        public virtual void Save()
        {
            _genericRepository.Save();
        }

        public virtual Task SaveAsync(CancellationToken token = default)
        {
            return _genericRepository.SaveAsync(token);
        }

        public virtual void Update(TEntity entity)
        {
            _genericRepository.Update(entity);
        }

        public virtual void Update(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null)
        {
            _genericRepository.Update(newValues, where);
        }

        public virtual Task UpdateAsync(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null, CancellationToken token = default)
        {
            return _genericRepository.UpdateAsync(newValues, where, token);
        }

        public virtual void UpdateMany(IEnumerable<TEntity> entities)
        {
            _genericRepository.UpdateMany(entities);
        }
    }
}